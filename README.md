# Notes


## What is this?
This repository contains notes and instructions on working with simulation codes, on supercoputing systems etc.
Everything you might easily forget, just put it in here.



## Prerequisites: 
``python3-sphinx`` and ``pandoc``, ``nbsphinx`` and ``sphinx_rtd_theme`` sphinx extensions. Can be installed with

```
 $ sudo apt install build-essential python3 python3-dev python3-pip python3-sphinx pandoc
 $ pip3 install nbsphinx sphinx_rtd_theme

```

on ubuntu/debian systems.

If you intend to use jupyter notebooks, you'll also have to install it using ``pip3 install jupyter`` or any other way you want.



## How does it work?
The documentation is built with sphinx. You'll need to install it first. See instructions above.

Sphinx uses 'restructured text' (reST). For tutorials, have a look [here](https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html) or [here](https://bitbucket.org/mivkov/coding_templates/src/master/Python/python3/sphinx/) .

To have a look at the content, you need to build it first. Navigate into the directory you're interested in (currently, there's only 'debugging' and 'instructions') and either run ``$ make html`` or ``$ ./quickbuild.sh``. To view the pages, then open the ``debugging/build/html/index.html`` or ``instructions/build/html/index.html`` pages with the web browser of your choice. (Easy way to do it from the terminal is `` $ firefox build/html/index.html`` )
You can have a look what the resulting [debugging](https://obswww.unige.ch/~ivkovic/debugging/) and [instructions](https://obswww.unige.ch/~ivkovic/instructions/) pages should look like when they're built.




Furthermore, including jupyter notebooks is possible and quite easy. Just include them in the ``toctree`` as if they were ``.rst`` files. You will however need the ``nbsphinx`` package (which you can install with ``$ pip3 install nbsphinx``) and ``pandoc`` (``$ sudo apt install pandoc``), and possibly others if you need the jupyter notebooks to be executed.
For more information on installation and how to work with sphinx and jupyter, look at the [documentation](https://nbsphinx.readthedocs.io/en/0.3.5/).






## Contributing
Please feel free to contribute! At the moment, I don't see a reason to prescribe exactly how to document whatever you want to document, but it would be nice to pay attention to some conventions:

- If you write something about e.g. GEAR, then please put it in the ``source/gear/`` directory. 
- Feel free to add new directories for other programs or libraries. Just don't forget to add the root file to the toctree.
- By default, jupyter notebooks aren't executed. This allows for code to be written and published that can't run on local machines (e.g. loading modules as you would on lesta) without plastering the whole notebook with error messages. If you want some documents to be executed, that should be possible by adding a little something to the file (see [documentation](https://nbsphinx.readthedocs.io/en/0.3.5/) ).
- For jupyter notebooks: Please make sure the cells where you write text are Markdown cells. (Also, in order to be able to add the notebook to the table of contents in a .rst file, which is necessary for the file to be included in the html page, the first cell should be a Markdown cell containing a title)
