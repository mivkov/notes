#!/bin/bash
# This script




#=============================================================================
# Automatically add last modified date according to last commit date
#=============================================================================

# first store all the files that are modified;
# don't want them to be added automatically to the commit
# without you noticing!

modfiles=`git ls-files -m`



# Add or update dates to the file

for f in `find . -name "*.rst"`; do

    lastline=`tail -n 1 $f`
    llstrip="$(echo -e "${lastline}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
    commdate=`git log -n 1 --date=short --pretty=format:%cd -- $f`

    if [[ "$llstrip" = "*Page last edited"* ]]; then
        sed -i "s/\*Page last edited [0-9]*-[0-9]*-[0-9]*\*/\*Page last edited ${commdate}\*/" $f
    else
        if [ ! -z $commdate ]; then
            echo "" >> $f
            echo "" >> $f
            echo '*Page last edited' "$commdate"'*' >> $f
        fi;
    fi;

    # hack when something goes wrong that I don't want to investigate right now
    # sometimes it skips out on the date and then the rst formatting complains
    sed -i "s/\*Page last edited \*//" $f

done


# Now you're done adding dates to the files.
# Quietly add the changed files to the last commit.
# But only do so for files that were included in the commits 
# since your branch deviated from the master,
# don't add untracked files without permission!

mybranch=`git rev-parse --abbrev-ref HEAD`
deviat=`git merge-base $mybranch master`

i=0
looping=true
while $looping; do
    i=$((i+1))
    cmtlist=`git log -n $i --format=%H`

    if [[ ${cmtlist} == *${deviat}* ]]; then
        looping=false
    fi
done

#cmtlist now contains all commit hashes since the branch deviated from the master.

added=false
for f in `git ls-files -m`; do
    comm=`git log -n 1 --pretty=format:%H -- $f`
    if [[ ${cmtlist} == *${comm}* ]]; then

        if [[ ${modfiles} == *${f}* ]]; then
            # if changes in file not commited
            continue
        else
            git add $f
            added=true
        fi
    fi
done


if $added; then
    echo 'Quickbuild script: amending commit to add new dates'
    git commit --amend --no-edit --quiet
else
    echo 'Quickbuild script: nothing to add'
fi




#=============================================================================
# Make clean then rebuild
#=============================================================================

rm `find . -name "*checkpoint.ipynb"` > /dev/null 2>&1
make clean
make html





exit 0
