.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Jan 2021; 
.. _lesta-core-dumps:



Freshly Compile Code doesn't run on slurm, just dumps core
----------------------------------------------------------


Happens because depending on the partition you chose on lesta, the
processors aren't the same.

**Solution**:

Add the compilation to your slurm script.


*Page last edited 2021-01-15*
