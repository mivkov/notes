.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Jan 2019; 
.. _jupyter-localhost-not-found:



jupyter-notebook: no connection to localhost:8888
--------------------------------------------------------------------


Happened to me when logging in to unige via x2go and then connecting to
lesta via ssh on a terminal and trying to run jupyter-notebook.

A tab in firefox is opened with the error message:


.. code-block:: none



    Unable to connect

    Firefox can’t establish a connection to the server at localhost:8888.

        The site could be temporarily unavailable or too busy. Try again in a few moments.
        If you are unable to load any pages, check your computer’s network connection.
        If your computer or network is protected by a firewall or proxy, make sure that Firefox is permitted to access the Web.




**Solution**:

This happens when you have firefox already open on unige, but are trying to use
jupyter from lesta. (Can't use jupyter on unige, gotta log in.)
Quit firefox on unige, and restart jupyter from lesta, and it all should be fine now.




*Page last edited 2019-09-17*
