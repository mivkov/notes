.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018; 
.. _gear_segfaults:

GEAR SEGFAULTS
-------------------------


Segfault right at start
~~~~~~~~~~~~~~~~~~~~~~~~~~

when executing gear:

.. code-block:: none


    $ gear params
    Note: This is a massively parallel code, but you are running with 1 processor only.
    Compared to an equivalent serial code, there is some unnecessary overhead.

    This is Gadget, version `2.0'.

    Running on 1 processors.
    Segmentation fault (core dumped)


when running with ``gdb``:

.. code-block:: none

    $ gdb --args gear params
    GNU gdb (Ubuntu 8.1-0ubuntu3) 8.1.0.20180409-git
    Copyright (C) 2018 Free Software Foundation, Inc.
    License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
    and "show warranty" for details.
    This GDB was configured as "x86_64-linux-gnu".
    Type "show configuration" for configuration details.
    For bug reporting instructions, please see:
    <http://www.gnu.org/software/gdb/bugs/>.
    Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.
    For help, type "help".
    Type "apropos word" to search for commands related to "word"...
    Reading symbols from gear...(no debugging symbols found)...done.
    (gdb) run
    Starting program: /home/mivkov/applications/gear/src/gear params.nophysics
    [Thread debugging using libthread_db enabled]
    Using host libthread_db library "/lib/x86_64-linux-gnu/libthread_db.so.1".
    Note: This is a massively parallel code, but you are running with 1 processor only.
    Compared to an equivalent serial code, there is some unnecessary overhead.

    This is Gadget, version `2.0'.

    Running on 1 processors.

    Program received signal SIGSEGV, Segmentation fault.
    _IO_vfprintf_internal (s=0x0, format=0x7ffffffe3f90 "InitCondFile", ' ' <repeats 23 times>, "minimal_cosmo.dat\n", 
        ap=ap@entry=0x7ffffffdf5d0) at vfprintf.c:1283
    1283	vfprintf.c: No such file or directory.
    (gdb) 


**SOLUTION**: 

old versions of gear don't check whether the directory where you want to write your output data already exists, and throws a segfault when trying to write in a directory that isn't there. If you have something like ``OutputDir         snap`` in your parameter file, make sure to create the ``snap`` directory before running gear.


*Page last edited 2018-12-17*
