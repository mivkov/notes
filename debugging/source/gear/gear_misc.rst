.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018; 
.. _gear_misc:

GEAR Miscellaneous Errors and Crashes
-----------------------------------------------


Hundreds of 'Error. I miss a value for tag <blablabla>'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sample output:

.. code-block:: none

    $ gear params
    Note: This is a massively parallel code, but you are running with 1 processor only.
    Compared to an equivalent serial code, there is some unnecessary overhead.

    This is Gadget, version `2.0'.

    Running on 1 processors.
    [removed a few hunred lines that start with
    Error in file params:   Tag 'XX' not allowed or multiple defined,
    because they contain a lot of unknown characters]

    Error. I miss a value for tag 'InitCondFile' in parameter file 'params'.
    Error. I miss a value for tag 'OutputDir' in parameter file 'params'.
    Error. I miss a value for tag 'SnapshotFileBase' in parameter file 'params'.
    Error. I miss a value for tag 'EnergyFile' in parameter file 'params'.
    Error. I miss a value for tag 'SystemFile' in parameter file 'params'.
    Error. I miss a value for tag 'CpuFile' in parameter file 'params'.
    Error. I miss a value for tag 'InfoFile' in parameter file 'params'.
    Error. I miss a value for tag 'LogFile' in parameter file 'params'.
    Error. I miss a value for tag 'TimingsFile' in parameter file 'params'.
    Error. I miss a value for tag 'RestartFile' in parameter file 'params'.
    Error. I miss a value for tag 'ResubmitCommand' in parameter file 'params'.
    Error. I miss a value for tag 'OutputListFilename' in parameter file 'params'.
    Error. I miss a value for tag 'OutputListOn' in parameter file 'params'.
    Error. I miss a value for tag 'Omega0' in parameter file 'params'.
    Error. I miss a value for tag 'OmegaBaryon' in parameter file 'params'.
    Error. I miss a value for tag 'OmegaLambda' in parameter file 'params'.
    Error. I miss a value for tag 'HubbleParam' in parameter file 'params'.
    Error. I miss a value for tag 'BoxSize' in parameter file 'params'.
    Error. I miss a value for tag 'PeriodicBoundariesOn' in parameter file 'params'.
    Error. I miss a value for tag 'TimeOfFirstSnapshot' in parameter file 'params'.
    Error. I miss a value for tag 'CpuTimeBetRestartFile' in parameter file 'params'.
    Error. I miss a value for tag 'TimeBetStatistics' in parameter file 'params'.
    Error. I miss a value for tag 'TimeBegin' in parameter file 'params'.
    Error. I miss a value for tag 'TimeMax' in parameter file 'params'.
    Error. I miss a value for tag 'TimeBetSnapshot' in parameter file 'params'.
    Error. I miss a value for tag 'UnitVelocity_in_cm_per_s' in parameter file 'params'.
    Error. I miss a value for tag 'UnitLength_in_cm' in parameter file 'params'.
    Error. I miss a value for tag 'UnitMass_in_g' in parameter file 'params'.
    Error. I miss a value for tag 'TreeDomainUpdateFrequency' in parameter file 'params'.
    Error. I miss a value for tag 'ErrTolIntAccuracy' in parameter file 'params'.
    Error. I miss a value for tag 'ErrTolTheta' in parameter file 'params'.
    Error. I miss a value for tag 'ErrTolForceAcc' in parameter file 'params'.
    Error. I miss a value for tag 'MinGasHsmlFractional' in parameter file 'params'.
    Error. I miss a value for tag 'MaxSizeTimestep' in parameter file 'params'.
    Error. I miss a value for tag 'MinSizeTimestep' in parameter file 'params'.
    Error. I miss a value for tag 'MaxRMSDisplacementFac' in parameter file 'params'.
    Error. I miss a value for tag 'ArtBulkViscConst' in parameter file 'params'.
    Error. I miss a value for tag 'CourantFac' in parameter file 'params'.
    Error. I miss a value for tag 'DesNumNgb' in parameter file 'params'.
    Error. I miss a value for tag 'MaxNumNgbDeviation' in parameter file 'params'.
    Error. I miss a value for tag 'ComovingIntegrationOn' in parameter file 'params'.
    Error. I miss a value for tag 'ICFormat' in parameter file 'params'.
    Error. I miss a value for tag 'SnapFormat' in parameter file 'params'.
    Error. I miss a value for tag 'NumFilesPerSnapshot' in parameter file 'params'.
    Error. I miss a value for tag 'NumFilesWrittenInParallel' in parameter file 'params'.
    Error. I miss a value for tag 'ResubmitOn' in parameter file 'params'.
    Error. I miss a value for tag 'TypeOfTimestepCriterion' in parameter file 'params'.
    Error. I miss a value for tag 'TypeOfOpeningCriterion' in parameter file 'params'.
    Error. I miss a value for tag 'TimeLimitCPU' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningHalo' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningDisk' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningBulge' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningGas' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningStars' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningBndry' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningHaloMaxPhys' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningDiskMaxPhys' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningBulgeMaxPhys' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningGasMaxPhys' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningStarsMaxPhys' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningBndryMaxPhys' in parameter file 'params'.
    Error. I miss a value for tag 'BufferSize' in parameter file 'params'.
    Error. I miss a value for tag 'PartAllocFactor' in parameter file 'params'.
    Error. I miss a value for tag 'TreeAllocFactor' in parameter file 'params'.
    Error. I miss a value for tag 'GravityConstantInternal' in parameter file 'params'.
    Error. I miss a value for tag 'InitGasTemp' in parameter file 'params'.
    Error. I miss a value for tag 'MinGasTemp' in parameter file 'params'.
    Error. I miss a value for tag 'RandomSeed' in parameter file 'params'.


**SOLUTION:** 

Did you really give gear a parameter file, or did you maybe by mistake give it the IC file as cmdline arg?













Can't determine next output time.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


.. code-block:: none

    This is Gear, version `2.0'.

    Running on 2 processors.

    Allocated 50 MByte communication buffer per processor.

    Communication buffer has room for 569878 particles in gravity computation
    Communication buffer has room for 192752 particles in density computation
    Communication buffer has room for 156038 particles in hydro computation
    Communication buffer has room for 142468 particles in domain decomposition


    Hubble (internal units) = 100
    G (internal units) = 43.0071
    Boltzmann = 6.94118e-70 
    ProtonMass = 8.40925e-68 
    mumh = 1.02552e-67 
    UnitMass_in_g = 1.989e+43 
    UnitTime_in_s = 3.08568e+19 
    UnitVelocity_in_cm_per_s = 100000 
    UnitDensity_in_cgs = 6.76991e-31 
    UnitEnergy_in_cgs = 1.989e+53 


    Using 77 as initial random seed
    Task=0  FFT-Slabs=128
    Task=1  FFT-Slabs=128
    Initialize cosmic table
    Initialize cosmic table done.
    Initialize full cosmic table
    Initialize full cosmic table done.

    Allocated 3.66455 MByte for particle storage. 152

    Allocated 0.507812 MByte for storage of SPH data. 208

    reading file `minimal_cosmo-GEAR.dat' on task=0 (contains 5056 particles.)
    distributing this file to tasks 0-1
    Type 0 (gas):        512  (tot=     0000000512) masstab=0.0381115
    Type 1 (halo):         0  (tot=     0000000000) masstab=0
    Type 2 (disk):       512  (tot=     0000000512) masstab=0
    Type 3 (bulge):        0  (tot=     0000000000) masstab=0
    Type 4 (stars):        0  (tot=     0000000000) masstab=0
    Type 5 (bndry):     4032  (tot=     0000004032) masstab=1.87001

    Reading block: POS 
    Reading block: VEL 
    Reading block: ID  
    Reading block: MASS
    Reading block: U   
    reading done.
    Total number of particles :  0000005056


    Minimum Time Step       (Timebase_interval) = 8.93286e-09 

    allocated 0.0762939 Mbyte for ngb search.

    Allocated 27.9579 MByte for BH-tree. 112

    start domain decomposition... 
    NTopleaves= 141
    work-load balance=1.0091   memory-balance=1.0091
    exchange of 0000002723 particles
    domain decomposition done. 
    begin Peano-Hilbert order...
    Peano-Hilbert done.
    Begin Ngb-tree construction.
    Ngb-Tree contruction finished 
    ngb iteration 1: need to repeat for 0000000512 particles.
    ngb iteration 2: need to repeat for 0000000512 particles.
    ngb iteration 3: need to repeat for 0000000512 particles.
    ngb iteration 4: need to repeat for 0000000512 particles.
    ngb iteration 5: need to repeat for 0000000511 particles.
    ngb iteration 6: need to repeat for 0000000499 particles.
    ngb iteration 7: need to repeat for 0000000394 particles.
    ngb iteration 8: need to repeat for 0000000191 particles.
    ngb iteration 9: need to repeat for 0000000129 particles.
    ngb iteration 10: need to repeat for 0000000093 particles.
    ngb iteration 11: need to repeat for 0000000035 particles.
    ngb iteration 12: need to repeat for 0000000001 particles.
    Initialize cosmic table
    Initialize cosmic table done.
    Initialize full cosmic table
    Initialize full cosmic table done.
    Can't determine next output time.
    task 0: endrun called with an error level of 110



**SOLUTION** 

This happened to me when running cosmological runs, i.e. with ``ComovingIntegrationOn    1`` in my parameter file. With cosmo runs, the output times are determined by multiplying the current timestep with the factor ``TimeBetSnapshots`` so that you get snapshots evenly spaced in :math:`\log a` space. Make sure ``TimeBetSnapshots`` and ``TimeBetStatistics`` are  >1 and  ``TimeBegin`` and ``TimeOfFirstSnapshot`` are > 0.



*Page last edited 2018-12-17*
