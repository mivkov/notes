.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018; 

.. _GEAR:

GEAR
========================


.. toctree::
    :maxdepth: 2

    gear_segfaults
    gear_misc


*Page last edited 2018-12-17*
