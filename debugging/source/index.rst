.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018; 
.. Welcome!
   sphinx-quickstart on Wed Apr  4 15:03:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Debugging Notes
===================================


A collection of encountered bugs, crashes, failures etc and instructions on how to deal with them.



.. toctree::
    :maxdepth: 2

    gadget/GADGET
    gear/GEAR
    latex/LATEX
    lesta/LESTA
    pnbody/PNBODY
    ramses/RAMSES
    spack_and_modules/spack_and_modules
    swift/SWIFT
    yt/YT
    template/template


*Page last edited 2019-09-17*
