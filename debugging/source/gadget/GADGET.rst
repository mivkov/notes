.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018; 

.. _GADGET:

GADGET
========================

.. toctree::
    :maxdepth: 2

    gadget_misc
    gadget_mpi
    gadget_segfaults





*Page last edited 2018-12-17*
