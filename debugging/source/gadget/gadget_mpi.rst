.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018; 
.. _gadget_mpi_issues:

Gadget MPI Issues
-----------------------


Gadget doesn't run in parallel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You run Gadget with ``mpirun -n X Gadget2 params``, but instead of launching one instance of Gadget on X processors, it is executed X times on a processor each

**Solution**: 

Did you load the same MPI module that you compiled gadget with? They need to be the same.



*Page last edited 2018-12-17*
