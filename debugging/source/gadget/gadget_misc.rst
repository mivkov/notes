.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018; 
.. _gadget_misc:

Gadget Miscellaneous Errors and Crashes
-----------------------------------------------


"Cannot create regular file 'something/something_else-usedvalues'"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sample output:

.. code-block:: none

    $ Gadget2 params.param

    Note: This is a massively parallel code, but you are running with 1 processor only.
    Compared to an equivalent serial code, there is some unnecessary overhead.

    This is Gadget, version `2.0'.

    Running on 1 processors.
    cp: cannot create regular file 'outputs/parameters-usedvalues': No such file or directory


    found 5 times in output-list.

    Allocated 30 MByte communication buffer per processor.

    Communication buffer has room for 604946 particles in gravity computation
    Communication buffer has room for 245760 particles in density computation
    Communication buffer has room for 196608 particles in hydro computation
    Communication buffer has room for 182890 particles in domain decomposition


    Hubble (internal units) = 0.1
    G (internal units) = 43007.1
    UnitMass_in_g = 1.989e+43 
    UnitTime_in_s = 3.08568e+16 
    UnitVelocity_in_cm_per_s = 100000 
    UnitDensity_in_cgs = 6.76991e-22 
    UnitEnergy_in_cgs = 1.989e+53 

    error in opening file 'outputs/cpu.txt'
    task 0: endrun called with an error level of 1

    --------------------------------------------------------------------------
    MPI_ABORT was invoked on rank 0 in communicator MPI_COMM_WORLD
    with errorcode 1.

    NOTE: invoking MPI_ABORT causes Open MPI to kill all MPI processes.
    You may or may not see output from other processes, depending on
    exactly when Open MPI kills them.
    --------------------------------------------------------------------------

**SOLUTION**: 

The output directory specified as ``OutputDir  outputs`` in the parameter file doesn't exist. Create it first, then run gadget.











Hundreds of 'Error. I miss a value for tag <blablabla>'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sample output:

.. code-block:: none


    Note: This is a massively parallel code, but you are running with 1 processor only.
    Compared to an equivalent serial code, there is some unnecessary overhead.

    This is Gadget, version `2.0'.

    Running on 1 processors.

    [removed a few hunred lines that start with
    Error in file params:   Tag 'XX' not allowed or multiple defined,
    because they contain a lot of unknown characters]

    Error. I miss a value for tag 'InitCondFile' in parameter file 'params'.
    Error. I miss a value for tag 'OutputDir' in parameter file 'params'.
    Error. I miss a value for tag 'SnapshotFileBase' in parameter file 'params'.
    Error. I miss a value for tag 'EnergyFile' in parameter file 'params'.
    Error. I miss a value for tag 'CpuFile' in parameter file 'params'.
    Error. I miss a value for tag 'InfoFile' in parameter file 'params'.
    Error. I miss a value for tag 'TimingsFile' in parameter file 'params'.
    Error. I miss a value for tag 'RestartFile' in parameter file 'params'.
    Error. I miss a value for tag 'ResubmitCommand' in parameter file 'params'.
    Error. I miss a value for tag 'OutputListFilename' in parameter file 'params'.
    Error. I miss a value for tag 'OutputListOn' in parameter file 'params'.
    Error. I miss a value for tag 'Omega0' in parameter file 'params'.
    Error. I miss a value for tag 'OmegaBaryon' in parameter file 'params'.
    Error. I miss a value for tag 'OmegaLambda' in parameter file 'params'.
    Error. I miss a value for tag 'HubbleParam' in parameter file 'params'.
    Error. I miss a value for tag 'BoxSize' in parameter file 'params'.
    Error. I miss a value for tag 'PeriodicBoundariesOn' in parameter file 'params'.
    Error. I miss a value for tag 'TimeOfFirstSnapshot' in parameter file 'params'.
    Error. I miss a value for tag 'CpuTimeBetRestartFile' in parameter file 'params'.
    Error. I miss a value for tag 'TimeBetStatistics' in parameter file 'params'.
    Error. I miss a value for tag 'TimeBegin' in parameter file 'params'.
    Error. I miss a value for tag 'TimeMax' in parameter file 'params'.
    Error. I miss a value for tag 'TimeBetSnapshot' in parameter file 'params'.
    Error. I miss a value for tag 'UnitVelocity_in_cm_per_s' in parameter file 'params'.
    Error. I miss a value for tag 'UnitLength_in_cm' in parameter file 'params'.
    Error. I miss a value for tag 'UnitMass_in_g' in parameter file 'params'.
    Error. I miss a value for tag 'TreeDomainUpdateFrequency' in parameter file 'params'.
    Error. I miss a value for tag 'ErrTolIntAccuracy' in parameter file 'params'.
    Error. I miss a value for tag 'ErrTolTheta' in parameter file 'params'.
    Error. I miss a value for tag 'ErrTolForceAcc' in parameter file 'params'.
    Error. I miss a value for tag 'MinGasHsmlFractional' in parameter file 'params'.
    Error. I miss a value for tag 'MaxSizeTimestep' in parameter file 'params'.
    Error. I miss a value for tag 'MinSizeTimestep' in parameter file 'params'.
    Error. I miss a value for tag 'MaxRMSDisplacementFac' in parameter file 'params'.
    Error. I miss a value for tag 'ArtBulkViscConst' in parameter file 'params'.
    Error. I miss a value for tag 'CourantFac' in parameter file 'params'.
    Error. I miss a value for tag 'DesNumNgb' in parameter file 'params'.
    Error. I miss a value for tag 'MaxNumNgbDeviation' in parameter file 'params'.
    Error. I miss a value for tag 'ComovingIntegrationOn' in parameter file 'params'.
    Error. I miss a value for tag 'ICFormat' in parameter file 'params'.
    Error. I miss a value for tag 'SnapFormat' in parameter file 'params'.
    Error. I miss a value for tag 'NumFilesPerSnapshot' in parameter file 'params'.
    Error. I miss a value for tag 'NumFilesWrittenInParallel' in parameter file 'params'.
    Error. I miss a value for tag 'ResubmitOn' in parameter file 'params'.
    Error. I miss a value for tag 'TypeOfTimestepCriterion' in parameter file 'params'.
    Error. I miss a value for tag 'TypeOfOpeningCriterion' in parameter file 'params'.
    Error. I miss a value for tag 'TimeLimitCPU' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningHalo' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningDisk' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningBulge' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningGas' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningStars' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningBndry' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningHaloMaxPhys' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningDiskMaxPhys' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningBulgeMaxPhys' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningGasMaxPhys' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningStarsMaxPhys' in parameter file 'params'.
    Error. I miss a value for tag 'SofteningBndryMaxPhys' in parameter file 'params'.
    Error. I miss a value for tag 'BufferSize' in parameter file 'params'.
    Error. I miss a value for tag 'PartAllocFactor' in parameter file 'params'.
    Error. I miss a value for tag 'TreeAllocFactor' in parameter file 'params'.
    Error. I miss a value for tag 'GravityConstantInternal' in parameter file 'params'.
    Error. I miss a value for tag 'InitGasTemp' in parameter file 'params'.
    Error. I miss a value for tag 'MinGasTemp' in parameter file 'params'.




**SOLUTION:** 

Did you really give gadget a parameter file, or did you maybe by mistake give it the IC file as cmdline arg?













Error while loading shared libraries: libsrfftw_mpi.so.2
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sample output:

.. code-block:: none

    Gadget2: error while loading shared libraries: libsrfftw_mpi.so.2: cannot open shared object file: No such file or directory

**SOLUTION**: 

You probably didn't load the FFTW2 module correctly, or it isn't in your path.














"I've discovered something odd. The mass content accounts only for Omega="
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sample output:

.. code-block:: none

    This is Gadget, version `2.0'.

    Running on 2 processors.

    found 5 times in output-list.

    Allocated 30 MByte communication buffer per processor.

    Communication buffer has room for 604946 particles in gravity computation
    Communication buffer has room for 245760 particles in density computation
    Communication buffer has room for 196608 particles in hydro computation
    Communication buffer has room for 182890 particles in domain decomposition


    Hubble (internal units) = 0.1
    G (internal units) = 43007.1
    UnitMass_in_g = 1.989e+43 
    UnitTime_in_s = 3.08568e+16 
    UnitVelocity_in_cm_per_s = 100000 
    UnitDensity_in_cgs = 6.76991e-22 
    UnitEnergy_in_cgs = 1.989e+53 

    Task=0  FFT-Slabs=64
    Task=1  FFT-Slabs=64

    Allocated 0.277328 MByte for particle storage. 80


    reading file `../music/minimal_cosmo.dat' on task=0 (contains 4544 particles.)
    distributing this file to tasks 0-1
    Type 0 (gas):          0  (tot=     0000000000) masstab=0
    Type 1 (halo):       512  (tot=     0000000512) masstab=0.233751
    Type 2 (disk):         0  (tot=     0000000000) masstab=0
    Type 3 (bulge):        0  (tot=     0000000000) masstab=0
    Type 4 (stars):        0  (tot=     0000000000) masstab=0
    Type 5 (bndry):     4032  (tot=     0000004032) masstab=1.87001

    reading done.
    Total number of particles :  0000004544



    I've found something odd!
    The mass content accounts only for Omega=2.7597e+08,
    but you specified Omega=0.276 in the parameterfile.

    

**SOLUTION**: 

Something went wrong with the units. Make sure you specified the units in the gadget file correctly. MUSIC uses the following default values:

.. code-block:: none

    UnitLength_in_cm            3.08568025e24   ; 1.0 Mpc
    UnitMass_in_g               1.989e43        ; 1.0e10 solar masses
    UnitVelocity_in_cm_per_s    1e5             ; 1 km/sec















Error: A timestep of size zero was assigned on the integer timeline!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


.. code-block:: none


    This is Gadget, version `2.0'.

    Running on 2 processors.

    found 4 times in output-list.

    Allocated 30 MByte communication buffer per processor.

    Communication buffer has room for 604946 particles in gravity computation
    Communication buffer has room for 245760 particles in density computation
    Communication buffer has room for 196608 particles in hydro computation
    Communication buffer has room for 182890 particles in domain decomposition


    Hubble (internal units) = 100
    G (internal units) = 43.0071
    UnitMass_in_g = 1.989e+43 
    UnitTime_in_s = 3.08568e+19 
    UnitVelocity_in_cm_per_s = 100000 
    UnitDensity_in_cgs = 6.76991e-31 
    UnitEnergy_in_cgs = 1.989e+53 

    Task=0  FFT-Slabs=64
    Task=1  FFT-Slabs=64

    Allocated 2.31392 MByte for particle storage. 80

    Allocated 0.175838 MByte for storage of SPH data. 84


    reading file `../music/minimal_cosmo.dat' on task=0 (contains 37913 particles.)
    distributing this file to tasks 0-1
    Type 0 (gas):       2744  (tot=     0000002744) masstab=0.00476394
    Type 1 (halo):      2744  (tot=     0000002744) masstab=0.0244549
    Type 2 (disk):         0  (tot=     0000000000) masstab=0
    Type 3 (bulge):        0  (tot=     0000000000) masstab=0
    Type 4 (stars):        0  (tot=     0000000000) masstab=0
    Type 5 (bndry):    32425  (tot=     0000032425) masstab=0.233751

    reading done.
    Total number of particles :  0000037913

    allocated 0.0762939 Mbyte for ngb search.

    Allocated 2.47529 MByte for BH-tree. 64

    domain decomposition... 
    NTopleaves= 106
    work-load balance=1.01105   memory-balance=1.01105
    exchange of 0000020507 particles
    domain decomposition done. 
    begin Peano-Hilbert order...
    Peano-Hilbert done.
    Begin Ngb-tree construction.
    Ngb-Tree contruction finished 
    ngb iteration 1: need to repeat for 0000000275 particles.

    Setting next time for snapshot file to Time_next= 0.25


    Begin Step 0, Time: 0.0909091, Redshift: 10, Systemstep: 0, Dloga: 0
    domain decomposition... 
    NTopleaves= 106
    work-load balance=1.00743   memory-balance=1.01105
    domain decomposition done. 
    begin Peano-Hilbert order...
    Peano-Hilbert done.
    Start force computation...
    Starting periodic PM calculation.

    Allocated 17.6931 MByte for FFT data.

    done PM.
    Tree construction.
    Tree construction done.
    Begin tree force.
    tree is done.
    Begin tree force.
    tree is done.
    Start density computation...
    Start hydro-force computation...
    force computation done.
    type=0  dmean=0.156244 asmth=0.0976562 minmass=0.00476394 a=0.0909091  sqrt(<p^2>)=5.69621  dlogmax=0.0543663
    type=1  dmean=0.156244 asmth=0.0976562 minmass=0.0244549 a=0.0909091  sqrt(<p^2>)=5.72259  dlogmax=0.0541157
    type=5  dmean=0.331589 asmth=0.0976562 minmass=0.233751 a=0.0909091  sqrt(<p^2>)=4.67455  dlogmax=0.0662485
    displacement time constraint: 0.03  (0.03)

    Error: A timestep of size zero was assigned on the integer timeline!
    We better stop.
    Task=0 Part-ID=5489 dt=-nan tibase=8.93286e-09 ti_step=-2147483648 ac=-nan xyz=(0.0393317|0.02582|0.267372) tree=(-nan|-nan-nan)

    pm_force=(-584.806|-562.549|-857.11)
    task 0: endrun called with an error level of 818


**SOLUTION**: 

Make sure all the softening parameters (``Softening*``) in your parameter file are non-zero. They are needed to compute the timestep.










No domain decomposition that stays within memory bounds is possible.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Gadget2 exits after message:

.. code-block:: none

    No domain decomposition that stays within memory bounds is possible.



**SOLUTION**: 

Increase ``*AllocFactor`` in your params file. (Or apparently run on more processors)


*Page last edited 2018-12-17*
