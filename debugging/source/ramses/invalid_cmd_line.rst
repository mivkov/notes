.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2019;


.. _ramses_invalid_cmd_line:


Fortran Runtime Error: EXECUTE_COMMAND_LINE: Invalid Command Line
-----------------------------------------------------------------------



**Error message:**

.. code-block:: none

    Error termination. Backtrace:
    #0  0x4006d9 in set_cmdstat
        at ../../../cray-gcc-6.2.0/libgfortran/intrinsics/execute_command_line.c:58
    #1  0x66e5d8 in set_cmdstat
        at ../../../cray-gcc-6.2.0/libgfortran/intrinsics/execute_command_line.c:89
    #2  0x66e5d8 in execute_command_line
        at ../../../cray-gcc-6.2.0/libgfortran/intrinsics/execute_command_line.c:112
    #3  0x525b37 in create_output_dirs_
        at ../amr/output_amr.f90:679
    #4  0x525e7b in dump_all_
        at ../amr/output_amr.f90:40
    #5  0x51c1a0 in amr_step_
        at ../amr/amr_step.f90:156
    #6  0x51a72b in adaptive_loop_
        at ../amr/adaptive_loop.f90:130
    #7  0x40a415 in ramses
        at ../amr/ramses.f90:13
    #8  0x40a415 in main
        at ../amr/ramses.f90:15
    srun: error: nid01210: task 0: Exited with exit code 2




**What's happening?**

No idea. It happens randomly, and the command line is correct.
It usually crashes after the merger tree and unbinding is done, i.e.
when it's called the second time for the same output, so the directory
is already created, hence the command line can't be invalid.

Possible reason, but haven't checked:
`StackExchange <https://stackoverflow.com/questions/55120720/fortran-execute-command-line-runtime-error-depends-on-memory-consumption>`_ .
Apparently ``call EXECUTE_COMMAND_LINE()`` creates a child process, copies all the memory, and crashes when it runs out of memory.
Possible that this is happening when clumpfinding arrays are still allocated and waiting for the dump...



**Solution**

Comment out the ``EXECUTE_COMMAND_LINE`` lines in the ``create_output_dirs`` routine and create the directories manually.


*Page last edited 2021-01-15*
