.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Jan 2019
.. _spack_hdf5:






==> Error: hdf5 does not depend on openmpi
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


.. code-block:: none
    
    $ spack install hdf5 ^openmpi
    ==> Error: hdf5 does not depend on openmpi


This happens because I added ``~mpi`` to the package configuration in ``~/.spack/packages.yaml`` for
hdf5 so I'll be able to install the non-mpi dependant
hdf5 libs as well.

Just remove it, install what you need, and put it back.


*Page last edited 2019-10-22*
