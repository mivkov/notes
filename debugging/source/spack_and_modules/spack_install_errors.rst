.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Dec 2018; 
.. _spack_install_errors:


Errors With Installations With Spack
-------------------------------------


'<package> needs both C and Fortran compilers'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Happened to me when trying to install ``mpi4py`` (for both mpich and openmpi).
``mpi4py`` needed ``gcc-8`` and ``gfortran-8``, which I had to manually install on ubuntu18.04
with ``sudo apt install gcc-8 gfortran-8 g++-8``. Using ``spack compiler find`` spack finds the
``gcc`` compiler, but not ``g++`` or ``gfortran``. You need to add the paths to the compiler
configuration file of spack manually: Running ``spack compiler find`` should tell you where the
file is. For me, it was ``/home/mivkov/.spack/linux/compilers.yaml``.

Find where the ``gcc-8`` compiler is listed in the file. For me, it looked like this:

.. code-block:: none

    - compiler:
        environment: {}
        extra_rpaths: []
        flags: {}
        modules: []
        operating_system: ubuntu18.04
        paths:
          cc: /usr/bin/gcc-8
          cxx: None
          f77: None
          fc: None
        spec: gcc@8.2.0
        target: x86_64

And replace it with the correct paths:

.. code-block:: none

    - compiler:
        environment: {}
        extra_rpaths: []
        flags: {}
        modules: []
        operating_system: ubuntu18.04
        paths:
          cc: /usr/bin/gcc-8
          cxx: /usr/bin/g++-8
          f77: /usr/bin/gfortran-8
          fc: /usr/bin/gfortran-8
        spec: gcc@8.2.0
        target: x86_64









==> Error: [Errno 13] Permission denied: '/-'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Happened to me when installing hdf5 for the second time with a new compiler (gcc@8.2.0) for ``py-h5py``:

.. code-block:: none

    $ spack install hdf5 %gcc@8.2.0
    ==> Installing libszip
    ==> Searching for binary cache of libszip
    ==> Warning: No Spack mirrors are currently configured
    ==> No binary for libszip found: installing from source
    ==> Error: [Errno 13] Permission denied: '/-'


A longer traceback can be found using ``spack -d install``:

.. code-block:: none

    $ spack -d install hdf5 %gcc@8.2.0
    ==> Reading config file /home/mivkov/local/spack/etc/spack/defaults/modules.yaml
    ==> Reading config file /home/mivkov/local/spack/etc/spack/defaults/linux/modules.yaml
    ==> Reading config file /home/mivkov/.spack/modules.yaml
    ==> Reading config file /home/mivkov/local/spack/etc/spack/defaults/config.yaml
    ==> Reading config file /home/mivkov/local/spack/etc/spack/defaults/repos.yaml
    ==> Reading config file /home/mivkov/local/spack/etc/spack/defaults/packages.yaml
    ==> Reading config file /home/mivkov/.spack/packages.yaml
    ==> READ LOCK: /home/mivkov/.spack/cache/providers/.builtin-index.yaml.lock[0:0] [Acquiring]
    ==> READ LOCK: /home/mivkov/.spack/cache/providers/.builtin-index.yaml.lock[0:0] [Acquired]
    ==> READ LOCK: /home/mivkov/.spack/cache/providers/.builtin-index.yaml.lock[0:0] [Released]
    ==> Reading config file /home/mivkov/.spack/linux/compilers.yaml
    ==> DATABASE LOCK TIMEOUT: 120s
    ==> PACKAGE LOCK TIMEOUT: No timeout
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/lock[0:0] [Acquiring]
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/lock[0:0] [Acquired]
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/lock[0:0] [Released]
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/lock[0:0] [Acquiring]
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/lock[0:0] [Acquired]
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/lock[0:0] [Released]
    ==> WRITE LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/prefix_lock[4185567495501045680:1] [Acquiring]
    ==> WRITE LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/prefix_lock[4185567495501045680:1] [Acquired]
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/lock[0:0] [Acquiring]
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/lock[0:0] [Acquired]
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/lock[0:0] [Released]
    ==> WRITE LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/prefix_lock[4185567495501045680:1] [Released]
    ==> link /home/mivkov/local/spack/var/spack/stage/hdf5-1.10.4-oqwd53arqw7wbcdta3uzmbv2qsgop5r3 -> /tmp/mivkov/spack-stage/spack-stage-zkpLCD
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/prefix_lock[4185567495501045680:1] [Acquiring]
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/prefix_lock[4185567495501045680:1] [Acquired]
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/prefix_lock[4185567495501045680:1] [Released]
    ==> Installing hdf5 dependencies
    ==> WRITE LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/prefix_lock[6227754510165278046:1] [Acquiring]
    ==> WRITE LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/prefix_lock[6227754510165278046:1] [Acquired]
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/lock[0:0] [Acquiring]
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/lock[0:0] [Acquired]
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/lock[0:0] [Released]
    ==> WRITE LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/prefix_lock[6227754510165278046:1] [Released]
    ==> link /home/mivkov/local/spack/var/spack/stage/libszip-2.1.1-vtnnw5r5ph5ly3hvz2oterymyzqixf54 -> /tmp/mivkov/spack-stage/spack-stage-ll1AAi
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/prefix_lock[6227754510165278046:1] [Acquiring]
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/prefix_lock[6227754510165278046:1] [Acquired]
    ==> READ LOCK: /home/mivkov/local/spack/opt/spack/.spack-db/prefix_lock[6227754510165278046:1] [Released]
    ==> Installing libszip
    ==> Searching for binary cache of libszip
    ==> Warning: No Spack mirrors are currently configured
    ==> No binary for libszip found: installing from source
    Traceback (most recent call last):
      File "/home/mivkov/local/spack/bin/spack", line 48, in <module>
        sys.exit(spack.main.main())
      File "/home/mivkov/local/spack/lib/spack/spack/main.py", line 669, in main
        return _invoke_command(command, parser, args, unknown)
      File "/home/mivkov/local/spack/lib/spack/spack/main.py", line 440, in _invoke_command
        return_val = command(parser, args)
      File "/home/mivkov/local/spack/lib/spack/spack/cmd/install.py", line 318, in install
        install_spec(args, kwargs, abstract, concrete)
      File "/home/mivkov/local/spack/lib/spack/spack/cmd/install.py", line 197, in install_spec
        install(spec, kwargs)
      File "/home/mivkov/local/spack/lib/spack/spack/cmd/install.py", line 186, in install
        spec.package.do_install(**kwargs)
      File "/home/mivkov/local/spack/lib/spack/spack/package.py", line 1402, in do_install
        **kwargs)
      File "/home/mivkov/local/spack/lib/spack/spack/package.py", line 1511, in do_install
        spack.store.layout.create_install_directory(self.spec)
      File "/home/mivkov/local/spack/lib/spack/spack/directory_layout.py", line 253, in create_install_directory
        mkdirp(spec.prefix, mode=perms)
      File "/home/mivkov/local/spack/lib/spack/llnl/util/filesystem.py", line 435, in mkdirp
        raise e
    OSError: [Errno 13] Permission denied: '/-'

The problem seems to be with some cache that spack has set up.
You can clean it up using ``$ spack clean -a``, and then try again.






*Page last edited 2019-10-22*
