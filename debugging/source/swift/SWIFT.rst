.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018; 

.. _SWIFT:

SWIFT
========================
    
.. toctree::
   :maxdepth: 2

   swift_runtime
   swift_compile
   swift_hdf5
   swift_mpi




*Page last edited 2019-10-24*
