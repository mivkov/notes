.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018; 

.. _swift_runtime:



SWIFT runtime errors
------------------------------

.. toctree::
   :maxdepth: 2

   swift_runtime_hydro_hdf5
   swift_runtime_error_while_loading_shared_libraries


*Page last edited 2019-08-19*
