.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Aug 2019; 


.. _runtime_hdf5_io:


hydro/GizmoMFV/hydro_io.h:202:7: runtime error: member access within null pointer of type 'const struct part'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




.. code-block:: none

    hydro/GizmoMFV/hydro_io.h:202:7: runtime error: member access within null pointer of type 'const struct part'
    hydro/GizmoMFV/hydro_io.h:205:13: runtime error: member access within null pointer of type 'const struct part'
    hydro/GizmoMFV/hydro_io.h:215:7: runtime error: member access within null pointer of type 'const struct part'
    hydro/GizmoMFV/hydro_io.h:218:13: runtime error: member access within null pointer of type 'const struct part'
    hydro/GizmoMFV/hydro_io.h:226:13: runtime error: member access within null pointer of type 'const struct part'


Code continues to run. No idea what the problem is, doesn't seem to
be a real problem actually. Code executes as expected otherwise. 
Other people can't reproduce it, so it's probably just a hdf5 installation/version issue.


*Page last edited 2019-08-19*
