
.. _swift_hdf5_error_opening_file:


Error opening file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


.. code-block:: none

    single_io.c:write_output_single():768: Error while opening file 'uniformPlane_0002.hdf5'.


The hdf5 file is already open/in use by some other process. Clean it up first and off you go.


*Page last edited 2019-08-23*
