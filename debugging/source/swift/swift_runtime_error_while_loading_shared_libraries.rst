.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Aug 2019; 


.. _runtime_error_shared_libraries:


error while loading shared libraries: X.so: cannot open shared object file: No such file or directory
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Example:

.. code-block:: none

    /home/astro/ivkovic/local/swiftsim//examples/swift-SPHM1RT-default: error while loading shared libraries: libsundials_cvode.so.5: cannot open shared object file: No such file or directory

To verify, you can check

.. code-block:: none

    $ ldd swift
        linux-vdso.so.1 =>  (0x00007ffe5b36b000)
        libgsl.so.23 => /astro/soft/common/spack/v20180409/spack/opt/spack/linux-centos7-x86_64/gcc-5.4.0/gsl-2.4-wnwmlz53mczc6z7zi6guxfebnibba77i/lib/libgsl.so.23 (0x00007f14ae51d000)
        [ ... lots of other lines ... ]
        libsundials_cvode.so.5 => none
        libsundials_nvecserial.so.5 => none
        libsundials_sunlinsoldense.so.3 => none
        libsundials_sunmatrixdense.so.3 => none
        [ ... lots of other lines ... ]
        libsz.so.2 => /lib64/libsz.so.2 (0x00007f14a9021000)
        libaec.so.0 => /lib64/libaec.so.0 (0x00007f14a8e19000)


Make sure you added the path to the external library you're trying to add to ``LD_LIBRARY_PATH``.




