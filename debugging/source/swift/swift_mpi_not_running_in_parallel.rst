.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018; 
.. _swift_not_running_in_parallel:


SWIFT doesn't run in parallel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You run SWIFT with ``mpirun -n X swift_mpi -a -b -c -d params.yaml``, but it is launched on 1 MPI rank nevertheless

**Solution**: 

Did you load the same MPI module that you compiled SWIFT with? They need to be the same.





*Page last edited 2019-08-19*
