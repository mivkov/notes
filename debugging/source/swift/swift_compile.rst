.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Oct 2019; 

.. _swift_compile:



SWIFT compiling errors
------------------------------

.. toctree::
   :maxdepth: 2

   swift_compile_implicit_conflicting


*Page last edited 2019-10-24*
