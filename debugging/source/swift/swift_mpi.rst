.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018; 
.. _swift_mpi_issues:



SWIFT MPI Issues
-----------------------


.. toctree::
    :maxdepth: 2

    swift_mpi_not_running_in_parallel


*Page last edited 2019-08-19*
