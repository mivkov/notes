.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Oct 2019; 

.. _swift_compile_implicit_conflicting:


``error: implicit declaration of function 'abc'`` and error: ``conflicting types for 'abc'``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: none

    /hydro/GizmoMFV/hydro_iact.h: In function 'runner_iact_density':
    ./hydro/GizmoMFV/hydro_iact.h:100:3: error: implicit declaration of function 'abc'; did you mean 'abs'? [-Werror=implicit-function-declaration]
       abc(pi, (int) pj->id, hidp1 * wi_dx * dx_mladen[0]/r, hidp1 * wi_dx * dx_mladen[1]/r, dx_mladen[0], dx_mladen[1], hidp1 * wi_dx, r, hi );
       ^~~
       abs
    In file included from todo_temporary_globals.c:4:
    todo_temporary_globals.h: At top level:
    todo_temporary_globals.h:68:13: error: conflicting types for 'abc' [-Werror]
     extern void abc(struct part *restrict pi,
                 ^~~
    In file included from hydro.h:60,
                     from todo_temporary_globals.h:8,
                     from todo_temporary_globals.c:4:
    ./hydro/GizmoMFV/hydro_iact.h:100:3: note: previous implicit declaration of 'abc' was here
       abc(pi, (int) pj->id, hidp1 * wi_dx * dx_mladen[0]/r, hidp1 * wi_dx * dx_mladen[1]/r, dx_mladen[0], dx_mladen[1], hidp1 * wi_dx, r, hi );
       ^~~




**Problem:** 

I added ``src/todo_temporary_globals.h`` and ``src/todo_temporary_globals.c`` files.
``.h`` had an ``extern void abc()`` declared, it is defined in ``.c``.

Then it is included as ``#include "todo_temporary_globals.h"`` in ``main.c``, and in ``hydro/GizmoMFV/hydro_iact.h``.

The problem was the ``todo_temporary_globals.h`` file had an ``#include "hydro.h"``. So it includes ``hydro.h``, which includes ``hydro_iact.h``, which again includes ``todo_temporary_globals.h`` again.




**Solution:**

move the ``#include "hydro.h"`` to the ``.c`` file. 
Then you skip the loop and it works.
Oddly enough the header file also doesn't complain about not having ``struct part`` defined anywhere.


*Page last edited 2019-10-24*
