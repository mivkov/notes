.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Dec 2018; 

.. _arxiv_image:

ArXiV Image not found
---------------------------------

Arxiv autotest fails with error message something like:

.. code-block:: none

    \includegraphics[]{images/tikz/potentials.pdf} 
    "images/tikz/potentials.pdf" not found


The problem here was that those files were pdfs created from tikz standalone files that were in the same directory.
For some reason, the uploader refused to upload the pdfs while .tex files with the same name were in the directory.
After removing the .tex files, all went well.


*Page last edited 2018-12-17*
