.. This file is part of the CLASTRO notes
.. Written by Mladen Ivkovic Oct 2019; 


.. _runner_functions:

Calling `runner_*` functions
--------------------------------------


You need to look in runner_doiact.h. the function names are assembled using macros. 
For example runner_iact_density comes from pasting density to runner_iact 
(runner_doiact.h:102) and then the macro IACT gets assigned to this which is called 
later on (for example line 349). The correct name gets passed in when including the 
header file multiple times with different definitions for the suffix 
(for example runner.c:87)


*Page last edited 2019-10-25*
