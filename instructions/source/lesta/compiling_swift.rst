.. This file is part of the CLASTRO instruction notes
   Original file written by Loic Hausammann
   This version written by Mladen Ivkovic Dec 2018
  


Running SWIFT on lesta
-------------------------


Compiling SWIFT
~~~~~~~~~~~~~~~~

 * First pull the last version of SWIFT (can't do this from lesta, internet connection is blocked. Do it from unige.)
 * On lesta load the following modules

.. code-block:: none

    1) spack/default   2) gcc/5.4.0   3) gsl/2.4        4) fftw/3.3.7
    5) jemalloc/4.5.0  6) metis/5.1.0 7) openmpi/2.1.0  8) numactl/2.0.11
    9) hdf5/1.10.0     10) cmake/3.12.1

You can make your life easier by just loading ``module load clastro spack swift cmake``.
  



Configuring SWIFT
~~~~~~~~~~~~~~~~~

 * First generate the configuration file `./autogen.sh`
 * You can check the available options with `./configure --help`

   * I am using the following command:
     `./configure --with-hdf5=$HDF5_ROOT/bin/h5pcc --with-gsl=$GSL_ROOT --with-metis=$METIS_ROOT --disable-parallel-hdf5 --with-numa=$NUMACTL_ROOT`
   * If you want to use velociraptor `--with-velociraptor=/home/epfl/lhausamm/VELOCIraptor-STF/src`

   * From what I have understood, NUMA is a library that deals with threads affinity. This means that the threads will be managed in a better way.
   * I think you need to disable the parallel hdf5 because the module was not compiled with it (you can try without but it should not really change anything on the small simulations).



Compiling SWIFT
~~~~~~~~~~~~~~~

 * To compile it simply use `make -j 8`
 * If you have some errors in numa.h, you will need to remove `Wstrict-prototypes` in the makefiles situated in `src`, `example` and `./`




Running SWIFT
~~~~~~~~~~~~~

  * For a full simulation (hydro, cosmo, gravity): `./swiftsim/examples/swift -a -c -G -s -x -t 56 full.yml`
    * `-a` is for using the thread affinity (need numa)
    * `-c` is for the cosmology
    * `-G` is for self gravity
    * `-x` is for velociraptor
    * `-t 56` is for the number of threads per MPI rank
    * `full.yml` is your parameters (you can copy/modify `swiftsim/examples/parameter_example.yml`)

 * To run with MPI
   * `srun ./swiftsim/examples/swift_mpi ...`

 * You can always check the available options with `./swiftsim/examples/swift --help`     


*Page last edited 2018-12-19*
