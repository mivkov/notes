.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018
.. Welcome!
   sphinx-quickstart on Wed Apr  4 15:03:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Miscellaneous Instructions
===================================


Various instructions and how-tos.



.. toctree::
    :maxdepth: 2

    gadget/GADGET
    gear/GEAR
    gitlab/GITLAB
    glups/GLUPS
    lesta/LESTA
    pnbody/PNBODY
    python/PYTHON
    ramses/RAMSES
    spack_and_modules/spack_and_modules
    swift/SWIFT
    yt/YT
    template/template





*Page last edited 2018-12-19*
