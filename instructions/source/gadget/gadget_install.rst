.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018
.. _install_gadget:

Installing Gadget
----------------------

- Download gadget from `here <https://wwwmpa.mpa-garching.mpg.de/gadget/gadget-2.0.7.tar.gz>`__.

- Make sure you have MPI, GSL, HDF5 and FFTW 2 installed. All but FFTW 2 can be easily installed and managed with :ref:`spack and modules<spack_and_modules>`. For FFTW 2, have a look at the instructions :ref:`here <installing_fftw2>`.

- Make sure you have all these modules loaded correctly if you're using ``lmod``.

- Navigate to the ``Gadget2`` directory (should contain a Makefile). If necessary, modify compilation flags and library paths. Mine look like this:

.. code-block:: none

    CC       =  mpicc
    OPTIMIZE =  -Wall -g -O3
    GSL_INCL =  -I/$(GSL_ROOT)/include
    GSL_LIBS =  -L/$(GSL_ROOT)/lib
    FFTW_INCL=  -I/$(FFTW_ROOT)/include 
    FFTW_LIBS=  -L/$(FFTW_ROOT)/lib
    HDF5INCL =  -I/$(HDF5_ROOT)/include
    HDF5LIB  =  -L/$(HDF5_ROOT)/lib  -lhdf5 -lz
    MPICHLIB =

- run ``make``




*Page last edited 2018-12-06*
