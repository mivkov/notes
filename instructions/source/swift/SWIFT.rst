.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018

.. _SWIFT:

SWIFT
========================
    
.. toctree::
   :maxdepth: 2

   swift_create_IC_from_gadget.ipynb
   swift_create_IC_from_arepo
   swift_misc



*Page last edited 2018-12-06*
