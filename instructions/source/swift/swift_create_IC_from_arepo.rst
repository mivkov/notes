.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Dec 2018


Converting AREPO initial conditions to SWIFT
-------------------------------------------------

According to Loic, AREPO also uses the hdf5 format.
Apparently he has a script on lesta in ``hpcstorage/lhausamm/astro3/music/arepo2swift.py``,
but it's a bit dirty, and you should check what the script does first.







*Page last edited 2018-12-17*
