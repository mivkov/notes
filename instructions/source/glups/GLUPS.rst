.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018
.. _glups:

glups
====================


Installing glups
-----------------------

You'll need a OpenGL library. Install on ubuntu:

.. code-block:: bash

   sudo apt update
   sudo apt install libglu1-mesa-dev freeglut3-dev mesa-common-dev


Then run ``setup.py``:

.. code-block:: bash

   python3 setup.py build
   python3 setup.py install --user



*Page last edited 2018-12-17*
