.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018

.. _PNBODY:

pNbody
========================


.. _install_pnbody:

Installing pNbody
--------------------

Install prerequisites:

.. code-block:: bash

   sudo apt install python-qt4 python3-sphinx
   pip3 install mpi4py astropy numpy matplotlib scipy sphinxcontrib.programoutput



Get ``pNbody`` from `gitlab <www.gitlab.com/revaz/pNbody>`_

.. code-block:: bash

    git clone https://gitlab.com/revaz/pNbody.git
    or
    git clone git@gitlab.com:revaz/pNbody.git

- Navigate to the downloaded directory and run

.. code-block:: bash

    cd pNbody
    python3 setup.py build
    python3 setup.py install --user

you can leave out ``--user`` flag if you want it installed for all users on your machine, but you'll probably need to ``sudo`` it first.



Building the Docs:

.. code-block:: bash

    cd Docs
    make html




Uninstalling pNbody
------------------------

If you installed ``pNbody`` the way described in :ref:`installing pnbody <install_pnbody>`, you can simply uninstall it with

.. code-block:: bash

    pip3 uninstall pNbody

**WARNING**: if you're in the ``pNbody`` directory, the command above will throw an error because there also is a directory called ``pNbody`` in there, like this: ::

   pip3 uninstall pNbody
   Can't uninstall 'pNbody'. No files were found to uninstall.

just go to any other direcoty, and the command should work.


*Page last edited 2018-12-06*
