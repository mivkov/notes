.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018
.. _gear_install:

Installing Gear
--------------------


- Download gear from gitlab

- go into the ``Gear/src/`` directory

- run

.. code-block:: none

    rm -rf CMakeCache.txt CMakeFiles
    cmake . -DOPTS=build_options/COSMO_FULL_CHEMISTRY_GRACKLE3_FFTW.txt
    make -j 8


the compilation options are located in ``Gear/src/build_options``


*Page last edited 2018-12-06*
