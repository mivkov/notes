.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018

.. _GEAR:

GEAR
========================


.. toctree::
    :maxdepth: 2

    gear_install
    gear_create_IC_from_gadget


*Page last edited 2018-12-06*
