.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Jan 2019; 
.. _lmod_misc:

Miscellanious lmod things
-----------------------------

Execute a command when loading a module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


You can tell spack/lmod to run specific commands when loading a module.
To do that, you'll need to modify the lmod files.
Example: I want to run ``mpirun`` with ``--mca mpi_cuda_support 0`` every time I have ``openmpi`` loaded to avoid annoying warning messages about cuda which I don't need.

Go and find the ``openmpi`` lua file in the ``spack`` directory. For me, it was ``~/local/spack/share/spack/lmod/linux-ubuntu18.04-x86_64/Core/``.
Then add ``set_alias('mpirun', "mpirun --mca mpi_cuda_support 0")`` to set the alias every time the module is loaded.
Don't forget to add aliases to all other mpi implementations to undo this when changing a module! E.g. ``set_alias('mpirun', "`which mpirun`")``

You can also execute any other command you want when loading the module. For more detail, have a look at `<https://lmod.readthedocs.io/en/latest/050_lua_modulefiles.html>`_








Get pretty names when using ``module avail``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Create the file ``~/.spack/modules.yaml`` with the following content:

.. code-block:: yaml

    modules:
      enable::
        - lmod
      lmod:
        core_compilers:
          - 'gcc@8.3.0'
        hierarchy:
          - mpi
        hash_length: 0
        blacklist:
          - autoconf
          - automake
          - bzip2
          - cmake
          - curl
          - diffutils
          - expat
          - gdbm
          - gettext
          - git
          - hwloc
          - libbsd
          - libiconv
          - libpciaccess
          - libtool
          - libxml2
          - lmod
          - libsigsegv
          - lua-luafilesystem
          - lua-luaposix
          - lua
          - m4
          - ncurses
          - numactl
          - openssl
          - pcre
          - perl
          - pkgconf
          - readline
          - tar
          - tcl
          - unzip
          - util-macros
          - xz
          - zlib



Then, remove the tcl module files so that they won't be shown: ``spack module tcl rm``

Finally, refresh the module files for lmod: ``spack module lmod refresh --delete-tree -y``

Now you should have nice and tidy module names.





*Page last edited 2019-12-31*
