.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018
.. _spack_and_modules:

SPACK and MODULES
====================

Spack is a package manager that makes your life easier when you need multiple versions of applications or libraries, e.g. multiple MPI versions, compiler versions etc.
Once you install spack, you can also install ``lmod``, which allows you to use modules the same way it is done on supercomuting systems.

.. toctree::
    :maxdepth: 2

    installing_spack
    installing_lmod
    installing_fftw2
    lmod_misc










*Page last edited 2019-10-22*
