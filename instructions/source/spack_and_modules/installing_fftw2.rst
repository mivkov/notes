.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018
.. _installing_fftw2:

Installing FFTW2 manually and including it in spack/lmod 
--------------------------------------------------------


Installing FFTW 2
~~~~~~~~~~~~~~~~~~~~~~~~~

A fix has been published on github, but for some reason not yet
merged into the master.
Anyhow, look at the ``file package.py`` in ``$SPACK_ROOT/var/spack/repos/builtin/packages/fftw/package.py`` and use `this one <https://github.com/spack/spack/commit/c42528fac2ae3620729b75c196662f97d665f88e>`_ instead.

Or have a look at `this <https://obswww.unige.ch/~ivkovic/debugging/spack_and_modules/spack_and_modules.html>`_.


**The notes below this line are deprecated, but should still work.**

For some reason, ``spack`` fails to install ``FFTW 2``. On my machine, I get this error:

.. code-block:: none

    $ spack install fftw@2.1.5
    ==> libsigsegv is already installed in /home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/libsigsegv-2.11-oromhtnsg3whjgy4ufgufwwwxe3iadp7
    ==> m4 is already installed in /home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/m4-1.4.18-vypfbpxo4do2ieh3hf7fbxpyz66b5zav
    ==> perl@5.26.1 : externally installed in /usr
    ==> perl@5.26.1 : already registered in DB
    ==> autoconf is already installed in /home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/autoconf-2.69-k35byc24q67run3egd2orhoaj54nmzb7
    ==> automake is already installed in /home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/automake-1.16.1-a2a6vcqlt6ayxo5wdmt4f4ksajhn4ihj
    ==> libtool is already installed in /home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/libtool-2.4.6-jher54m7efbgqjarpywxcb3yxmelyswk
    ==> texinfo is already installed in /home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/texinfo-6.5-hupxyymn5eiwtzg42j7phsl574nys3kl
    ==> findutils is already installed in /home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/findutils-4.6.0-capl6zymtrqxltqnfqiykagcciyumy6z
    ==> mpich is already installed in /home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/mpich-3.2.1-goluh6hkr5xlspftwizqxlysbdtghk67
    ==> Installing fftw
    ==> Using cached archive: /home/mivkov/applications/spack/var/spack/cache/fftw/fftw-2.1.5.tar.gz
    ==> Staging archive: /home/mivkov/applications/spack/var/spack/stage/fftw-2.1.5-wkxg7i7u6rsmzvtllkzob2ge5dx5q4lq/fftw-2.1.5.tar.gz
    ==> Created stage in /home/mivkov/applications/spack/var/spack/stage/fftw-2.1.5-wkxg7i7u6rsmzvtllkzob2ge5dx5q4lq
    ==> No patches needed for fftw
    ==> Building fftw [AutotoolsPackage]
    ==> Executing phase: 'autoreconf'
    ==> Executing phase: 'configure'
    ==> Executing phase: 'build'
    ==> Error: ProcessError: Command exited with status 2:
        'make' '-j8'

    4 errors found in build log:
         1623    /bin/bash ../libtool --mode=link /home/mivkov/applications/spack/lib/spack
                 /env/gcc/gcc  -O3 -fomit-frame-pointer -fno-schedule-insns -fschedule-insn
                 s2 -malign-double -fstrict-aliasing -pthread   -o fftw_test  fftw_test.o t
                 est_main.o ../fftw/libdfftw.la -lm
         1624    mkdir .libs
         1625    /home/mivkov/applications/spack/lib/spack/env/gcc/gcc -O3 -fomit-frame-poi
                 nter -fno-schedule-insns -fschedule-insns2 -malign-double -fstrict-aliasin
                 g -pthread -o .libs/fftw_test fftw_test.o test_main.o  ../fftw/.libs/libdf
                 ftw.so -lm -Wl,--rpath -Wl,/home/mivkov/applications/spack/opt/spack/linux
                 -ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-wkxg7i7u6rsmzvtllkzob2ge5dx5q4lq/
                 lib
         1626    creating fftw_test
         1627    /bin/bash ../libtool --mode=link /home/mivkov/applications/spack/lib/spack
                 /env/gcc/gcc  -O3 -fomit-frame-pointer -fno-schedule-insns -fschedule-insn
                 s2 -malign-double -fstrict-aliasing -pthread   -o rfftw_test  rfftw_test.o
                  test_main.o ../rfftw/libdrfftw.la ../fftw/libdfftw.la -lm
         1628    /home/mivkov/applications/spack/lib/spack/env/gcc/gcc -O3 -fomit-frame-poi
                 nter -fno-schedule-insns -fschedule-insns2 -malign-double -fstrict-aliasin
                 g -pthread -o .libs/rfftw_test rfftw_test.o test_main.o  ../rfftw/.libs/li
                 bdrfftw.so ../fftw/.libs/libdfftw.so -lm -Wl,--rpath -Wl,/home/mivkov/appl
                 ications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-wkx
                 g7i7u6rsmzvtllkzob2ge5dx5q4lq/lib
      >> 1629    ../rfftw/.libs/libdrfftw.so: undefined reference to `fftw_reverse_int_arra
                 y'
      >> 1630    collect2: error: ld returned 1 exit status
         1631    Makefile:212: recipe for target 'rfftw_test' failed
      >> 1632    make[1]: *** [rfftw_test] Error 1
         1633    make[1]: Leaving directory '/tmp/mivkov/spack-stage/spack-stage-R5BBIO/fft
                 w-2.1.5/double/tests'
         1634    Makefile:214: recipe for target 'all-recursive' failed
      >> 1635    make: *** [all-recursive] Error 1

    See build log for details:
      /home/mivkov/applications/spack/var/spack/stage/fftw-2.1.5-wkxg7i7u6rsmzvtllkzob2ge5dx5q4lq/fftw-2.1.5/spack-build.out 


So I decided to manually add FFTW 2.1.5 to ``spack`` and ``lmod``:

First download FFTW 2:

.. code-block:: bash

    $ wget http://www.fftw.org/fftw-2.1.5.tar.gz

Then untar it:

.. code-block:: bash

    $ tar -xvf fftw-2.1.5.tar.gz

Next decide how to name the directory correctly. I put it where the other FFTW libraries are, which I installed with SPACK. You can check where that is by looking at ``$ echo $FFTW_ROOT``.
For me, it was ``/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-3.3.8-grbvci4ohu36z4qyg6odi5k5fwt7zi77``, so I will put it in ``/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa``

Apparently spack uses 32 character long hashes to identify the installs, so just add 32 a's to the directory name or whatever you want. I'm just an instruction, not the police.

This particular install is with the ``openmpi`` library loaded, and later I repeat all the steps with ``mpich``. That's why I added 'openmpi' to the directory name. **Make sure you loaded the correct MPI version**.

Just using the ``configure`` and ``make`` for the FFTW2 library might fail to create the documentation; FFTW2 probably relies on some old version of texinfo. But it looks like you can safely ignore that. Let's be honest, none of us is gonna look for the dox in these directories.

The error I was getting looked like this:



.. code-block:: none


    /bin/bash ../mkinstalldirs /home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/include
     /usr/bin/install -c -m 644 drfftw.h /home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/include/drfftw.h
    make[2]: Leaving directory '/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/rfftw'
    make[1]: Leaving directory '/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/rfftw'
    Making install in tests
    make[1]: Entering directory '/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/tests'
    make[2]: Entering directory '/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/tests'
    make[2]: Nothing to be done for 'install-exec-am'.
    make[2]: Nothing to be done for 'install-data-am'.
    make[2]: Leaving directory '/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/tests'
    make[1]: Leaving directory '/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/tests'
    Making install in doc
    make[1]: Entering directory '/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/doc'
    /bin/bash /home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/missing --run makeinfo   -I . \
     -o fftw.info `test -f 'fftw.texi' || echo './'`fftw.texi
    fftw.texi:49: misplaced {
    fftw.texi:49: misplaced }
    fftw.texi:51: misplaced {
    fftw.texi:51: misplaced }
    fftw.texi:52: misplaced {
    fftw.texi:52: misplaced }
    Makefile:188: recipe for target 'fftw.info' failed
    make[1]: *** [fftw.info] Error 1
    make[1]: Leaving directory '/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa/doc'
    Makefile:214: recipe for target 'install-recursive' failed
    make: *** [install-recursive] Error 1



Easiest way to bypass it is to remove ``doc`` from the ``SUBDIRS`` variable in the ``Makefile`` that ``configure`` creates. Also, to install both single and double precision libraries, we'll have to configure and compile the library twice, once using ``--enable-float``, once not. To have both libraries simultaneously, we need to add ``--enable-type-prefix``. To save some work and thinking, just pack the following code into a bash script and execute it. Don't forget to adapt the ``FFTW2_ROOT`` appropriately to your situation.



.. code-block:: bash

    #!/bin/bash

    FFTW2_ROOT="/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"

    make distclean
    make uninstall

    ./configure --enable-float \
                --enable-type-prefix \
                --enable-threads \
                --enable-mpi \
                --prefix=$FFTW2_ROOT

    sed -i 's/SUBDIRS = fftw rfftw tests doc threads mpi/SUBDIRS = fftw rfftw tests threads mpi/' Makefile
    make -j 8
    make install


    ./configure --enable-type-prefix \
                --enable-threads \
                --enable-mpi \
                --prefix=$FFTW2_ROOT

    sed -i 's/SUBDIRS = fftw rfftw tests doc threads mpi/SUBDIRS = fftw rfftw tests threads mpi/' Makefile
    make -j 8
    make install









Adding FFTW 2 to modules and spack
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



Make your life easier and copy the ``../fftw-3.3.8-blablabla/.spack`` directory into your ``fftw-2.1.5-blabla`` directory. Then change file by file:

.. ``build.env``: Find and change following 4 lines:
.. .. code-block:: bash
..     export SPACK_LINK_DEPS="/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/mpich-3.2.1-goluh6hkr5xlspftwizqxlysbdtghk67"
..     export SPACK_PREFIX="/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-3.3.8-grbvci4ohu36z4qyg6odi5k5fwt7zi77"
..     export SPACK_RPATH_DEPS="/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/mpich-3.2.1-goluh6hkr5xlspftwizqxlysbdtghk67"
..     export SPACK_SHORT_SPEC="fftw@3.3.8%gcc@7.3.0+double+float~fma+long_double+mpi~openmp~pfft_patches~quad simd=avx,avx2,sse2 arch=linux-ubuntu18.04-x86_64 /grbvci4"
..
.. into:
.. .. code-block:: bash

..   export SPACK_LINK_DEPS="/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/openmpi-3.1.3-t35vifchtdehybj232idkc4hkcjh4p54/"
..     export SPACK_PREFIX="/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
..     export SPACK_RPATH_DEPS="/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/openmpi-3.1.3-t35vifchtdehybj232idkc4hkcjh4p54/"
..     export SPACK_SHORT_SPEC="fftw@2.1.5%gcc@7.3.0+double+float~fma+long_double+mpi~openmp~pfft_patches~quad simd=avx,avx2,sse2 arch=linux-ubuntu18.04-x86_64 /aaaaaaa"
..
..
.. line 1: change to your MPI directory
..
.. line 2: change to your fftw-2.1.5 directory
..
.. line 3: MPI version again
..
.. line 4: FFTW version, last short identifier into all a's


For ``build.env``, you can just copy-paste the file from the ``fftw-3.3.8-blablabla/.spack`` directory. Just pay attention that you grab the file from the fftw3 version with the right MPI version.
But you probably can ignore this file completely, as well as ``build.out``.



``spec.yaml``:

.. code-block:: yaml

    spec:
    - fftw:
        version: 2.1.5
        arch:
          platform: linux
          platform_os: ubuntu18.04
          target: x86_64
        compiler:
          name: gcc
          version: 7.3.0
        namespace: builtin
        parameters:
          double: true
          float: true
          fma: false
          long_double: true
          mpi: true
          openmp: false
          pfft_patches: false
          quad: false
          simd:
          - avx
          - avx2
          - sse2
          cflags: []
          cppflags: []
          cxxflags: []
          fflags: []
          ldflags: []
          ldlibs: []
        dependencies:
          mpich:
            hash: goluh6hkr5xlspftwizqxlysbdtghk67
            type:
            - build
            - link
        hash: grbvci4ohu36z4qyg6odi5k5fwt7zi77
    - mpich:
        version: 3.2.1
        arch:
          platform: linux
          platform_os: ubuntu18.04
          target: x86_64
        compiler:
          name: gcc
          version: 7.3.0
        namespace: builtin
        parameters:
          device: ch3
          hydra: true
          netmod: tcp
          pmi: true
          romio: true
          verbs: true
          cflags: []
          cppflags: []
          cxxflags: []
          fflags: []
          ldflags: []
          ldlibs: []
        hash: goluh6hkr5xlspftwizqxlysbdtghk67

change the hashes and versions appropriately. For me, it looks like this:

.. code-block:: yaml

    spec:
    - fftw:
        version: 3.3.8
        arch:
          platform: linux
          platform_os: ubuntu18.04
          target: x86_64
        compiler:
          name: gcc
          version: 7.3.0
        namespace: builtin
        parameters:
          double: true
          float: true
          fma: false
          long_double: true
          mpi: true
          openmp: false
          pfft_patches: false
          quad: false
          simd:
          - avx
          - avx2
          - sse2
          cflags: []
          cppflags: []
          cxxflags: []
          fflags: []
          ldflags: []
          ldlibs: []
        dependencies:
          openmpi:
          mpich:
            hash: t35vifchtdehybj232idkc4hkcjh4p54
            type:
            - build
            - link
        hash: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
    - openmpi:
        version: 3.1.3
        arch:
          platform: linux
          platform_os: ubuntu18.04
          target: x86_64
        compiler:
          name: gcc
          version: 7.3.0
        namespace: builtin
        parameters:
          device: ch3
          hydra: true
          netmod: tcp
          pmi: true
          romio: true
          verbs: true
          cflags: []
          cppflags: []
          cxxflags: []
          fflags: []
          ldflags: []
          ldlibs: []
        hash: t35vifchtdehybj232idkc4hkcjh4p54



Then also go and look for the file in ``$SPACK_ROOT/opt/spack/.spack-db/index.json``, copy-paste the entry for ``fftw 3``, and change it appropriately. For me that was

.. code-block:: none

    "grbvci4ohu36z4qyg6odi5k5fwt7zi77": {
    "explicit": true,
    "installation_time": 1541508606.593818,
    "ref_count": 0,
    "installed": true,
    "path": "/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-3.3.8-grbvci4ohu36z4qyg6odi5k5fwt7zi77",
    "spec": {
     "fftw": {
      "version": "3.3.8",
      "arch": {
       "platform": "linux",
       "platform_os": "ubuntu18.04",
       "target": "x86_64"
      },
      "compiler": {
       "name": "gcc",
       "version": "7.3.0"
      },
      "namespace": "builtin",
      "parameters": {
       "double": true,
       "float": true,
       "fma": false,
       "long_double": true,
       "mpi": true,
       "openmp": false,
       "pfft_patches": false,
       "quad": false,
       "simd": [
        "avx",
        "avx2",
        "sse2"
       ],
       "cflags": [],
       "cppflags": [],
       "cxxflags": [],
       "fflags": [],
       "ldflags": [],
       "ldlibs": []
      },
      "dependencies": {
       "mpich": {
        "hash": "goluh6hkr5xlspftwizqxlysbdtghk67",
        "type": [
         "build",
         "link"
        ]
       }
      }
     }
    }
   },


into:

.. code-block:: none

    "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa": {
    "explicit": true,
    "installation_time": 1541508606.593818,
    "ref_count": 0,
    "installed": true,
    "path": "/home/mivkov/applications/spack/opt/spack/linux-ubuntu18.04-x86_64/gcc-7.3.0/fftw-2.1.5-aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
    "spec": {
     "fftw": {
      "version": "2.1.5",
      "arch": {
       "platform": "linux",
       "platform_os": "ubuntu18.04",
       "target": "x86_64"
      },
      "compiler": {
       "name": "gcc",
       "version": "7.3.0"
      },
      "namespace": "builtin",
      "parameters": {
       "double": true,
       "float": true,
       "fma": false,
       "long_double": true,
       "mpi": true,
       "openmp": false,
       "pfft_patches": false,
       "quad": false,
       "simd": [
        "avx",
        "avx2",
        "sse2"
       ],
       "cflags": [],
       "cppflags": [],
       "cxxflags": [],
       "fflags": [],
       "ldflags": [],
       "ldlibs": []
      },
      "dependencies": {
       "openmpi": {
        "hash": "35vifchtdehybj232idkc4hkcjh4p54",
        "type": [
         "build",
         "link"
        ]
       }
      }
     }
    }
    },



Finally, find where the module files for ``fftw3`` are. You can see that by typing ``module avail`` and looking in the title. For me it was

.. code-block:: none

    $ module avail

    ------------- /home/mivkov/applications/spack/share/spack/lmod/linux-ubuntu18.04-x86_64/mpich/3.2.1-goluh6h/Core -------------
       fftw/3.3.8 (L)    grackle/3.1 (L)    hdf5/1.10.4 (L)    parmetis/4.0.3 (L)

    ----------------------- /home/mivkov/applications/spack/share/spack/lmod/linux-ubuntu18.04-x86_64/Core -----------------------
       cmake/3.12.3       git/2.17.1          jemalloc/4.5.0 (L)    metis/5.1.0   (L)    tcl/8.6.8
       cuda/10.0.130      gsl/2.5      (L)    libtool/2.4.6         mpich/3.2.1   (L)    texinfo/5.1
       diffutils/3.6      hdf/4.2.13          lmod/7.8              openmpi/3.1.3        texinfo/6.5 (D)
       findutils/4.6.0    hwloc/1.11.9        lua/5.3.4             perl/5.26.1          unzip/6.0

      Where:
       L:  Module is loaded
       D:  Default Module

    Use "module spider" to find all possible modules.
    Use "module keyword key1 key2 ..." to search for all possible modules matching any of the "keys".


So I see that fftw/3.3.8 with ``mpich`` is in ``/home/mivkov/applications/spack/share/spack/lmod/linux-ubuntu18.04-x86_64/mpich/3.2.1-goluh6h/Core``.

Go to the directory of the appropriate MPI version you used, copy-pase the ``3.3.8.lua`` file to ``2.1.5.lua`` file, then search and replace all ``3.3.8`` with ``2.1.5`` and the hashes you set.


Finally, reaload the module tree to see the changes:

.. code-block:: bash

   spack module tcl refresh -y


Now you should see ``fftw/2.1.5`` when using ``module avail``.



*Page last edited 2019-10-22*
