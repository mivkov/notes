.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018
.. _installing_spack:

Installing and configuring Spack
--------------------------------------



- download :ref:_`spack <https://spack.io/>` and follow the installation instructions.
- create a ``~/.spack`` directory if it doesn't exist yet
- create a file ``~/.spack/packages.yaml`` if it doesn't exist yet. There you can add any package specifications you want. Like which versions to install, or which packages not to install, but use the package inside your normal system (see ``git`` for example in the file below). My (Loic's) file looks like this:

.. code-block:: yaml

    packages:
        all:
            providers:
                mpi: [mpich, openmpi]
        
        git:
            paths:
                git@2.17.1: /usr/bin/git/

        perl:
            paths:
                perl@5.26.1: /usr/

        paraview:
            variants: +qt

        qt:
            variants: +opengl+gtk

        cairo:
            variants: +X

        gtkplus:
            variants: +X

        pango:
            variants: +X

        openmpi:
            variants: +cuda+thread_multiple

        hwloc:
            variants: +cuda

        hdf5:
            variants: +cxx+fortran+hl+szip+threadsafe

        mesa:
            variants: +hwrender~llvm

        llvm:
            variants: +python+link_dylib

        binutils:
            variants: +plugins

        mpich:
            variants: +verbs
      
        python:
            paths:
                python+optimizations+tk@3.7.0%gcc@8.2.0: /usr/python/python3.7.0
                python+tk+ucs4@2.7.15%gcc@8.2.0: /usr/python/python2.7.15
            buildable: False

- Add the ``spack`` executable to your ``$PATH`` variable (assuming you're using ``bash``): Add the following lines to your ``~/.bashrc`` file:

.. code-block:: bash

    export SPACK_ROOT="/path/to/where/you/installed/spack/"
    export PATH="$SPACK_ROOT/bin":"$PATH"




*Page last edited 2018-12-06*
