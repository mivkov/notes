.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018
.. _installing_lmod:


Installing and Configuring LMOD
-------------------------------------

- install ``lmod``: ``$ spack install lmod`` . If it doesn't work because ``spack`` is an unknown command: Did you set the ``$PATH`` variable correctly? Did you also re-source the ``.bashrc`` file to update your ``$PATH`` variable with ``$ source ~/.bashrc`` ?

- create ``~/.spack/modules.yaml`` file with the following content:

.. code-block:: yaml

    modules:
      enable::
        - lmod
      lmod:
        core_compilers:
          - 'gcc@8.3.0'
        hierarchy:
          - mpi
        hash_length: 0
        blacklist:
          - autoconf
          - automake
          - bzip2
          - cmake
          - curl
          - diffutils
          - expat
          - gdbm
          - gettext
          - git
          - hwloc
          - libbsd
          - libiconv
          - libpciaccess
          - libtool
          - libxml2
          - lmod
          - libsigsegv
          - lua-luafilesystem
          - lua-luaposix
          - lua
          - m4
          - ncurses
          - numactl
          - openssl
          - pcre
          - perl
          - pkgconf
          - readline
          - tar
          - tcl
          - unzip
          - util-macros
          - xz
          - zlib



In the ``blacklist`` section, you can add any library/program that you don't want to see when you use ``module avail``.

- add this **to the beginning of your** ``~/.bashrc`` **file. It needs to be done before anything else.**

.. code-block:: bash


    #!/bin/bash
     
    # NEEDS TO BE DONE AFTER PATH ADDITIONS AND BEFORE EVERYTHING ELSE

    export LMOD_PATH=$(spack location -i lmod)/lmod/lmod
    source $LMOD_PATH/init/bash

    # this path will be created once you install mpi version(s)
    export MODULEPATH=$SPACK_ROOT/share/spack/lmod/linux-ubuntu18.04-x86_64/Core

    if [ -d /etc/profile.d ]; then
     for i in /etc/profile.d/*.sh; do
       if [ -r $i ]; then
         . $i
       fi
     done
    fi

    export LMOD_CMD=$LMOD_PATH/libexec/lmod

    module()
    {
      eval $($LMOD_CMD bash $*)
    }

    # for spack/modules autocompletion
    source $SPACK_ROOT/share/spack/setup-env.sh





The directory ``linux-ubuntu18.04-x86_64`` may be different in your case. Check how the directories are actually called on your system.
Note also that this relies on the previously defined ``$SPACK_ROOT`` variable.

- re-source your ``~/.bashrc`` again with ``source ~/.bashrc`` and you're ready to go! Anything you install now with spack will turn up as a module that you can load/unload.


If you only want to use ``lmod``, but not any ``tcl`` module files, run ``spack module tcl rm`` once and ``spack module lmod refresh`` to properly generate ``lmod`` module files.

You can add some modifications like the blacklist for module files etc in ``~/.spack/modules.yaml``

*Page last edited 2019-12-31*
