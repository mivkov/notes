.. This file is part of the CLASTRO instruction notes
.. Written by Mladen Ivkovic Nov 2018


Sync build html documentation to obswww.unige.ch/~user
------------------------------------------------------------


Summary: Create artifacts with every push on gitlab using the continuous integration, 
during which the html documents are built as a test. The arefacts contain the html 
pages. Make them publicly downloadable, and use a cron job on obswww.unige to sync
the newest html version daily/weekly. Artifacts are created only for a successful build,
and kept on gitlab.com indefinitely.


Step-by-step:

- in your ``.gitlab-ci.yml`` file, define a job ``jobname_1`` that builds the documentation, e.g.

.. code-block:: yaml

    jobname_1:
        stage: build
        script:
        - make html
        - mkdir -p public
        - mv build/html/* public/
        artifacts:
            paths:
            - public
        only:
        - pushes

- the artifact should now be downloadable via the following link: ``https://gitlab.com/USERNAME/PROJECTNAME/-/jobs/artifacts/master/download?job=jobname_1``. Note that you'll have to insert your own ``USERNAME`` and ``PROJECTNAME``. Also note that ``jobname_1`` has to correspond to the job name you defined in your ``.gitlab-ci.yml`` file. (If you want to have a look at your artifacts in your browser instead of downloading them, replace ``download`` with ``raw`` in the link. By default, artifacts on gitlab.com are stored indefinitely.) ``master`` refers to the branch, so you can access the artifact from other branches by replacing ``master`` with the corresponding branch name.

- you can write a bash script to download the file and move them to your ``/www/`` directory:

.. code-block:: bash

    wd=`pwd`
    cd /tmp
    rm artifacts.zip # in case there is already artifacts.zip here, curl will fail
    curl -O -J -L https://gitlab.com/USERNAME/PROJECTNAME/-/jobs/artifacts/master/download?job=JOBNAME
    unzip artifacts.zip
    if [[ $? -ne 0 ]]; then continue; fi;
    cp -r public/$page/* /www/people/USERNAME/public/WHEREVER_YOU_WANT_IT
    rm -r public/
    cd $wd
   

- Make a `cron job <https://www.cyberciti.biz/faq/how-do-i-add-jobs-to-cron-under-linux-or-unix-oses/>`_ on the Geneva server to run your script once a day/week.


**Other options:**

- store your ssh password for unige as a secret variable on gitlab, and use sshpass in the ``.gitlab-ci.yml`` file to ``scp`` the new html version after every build. Detailed instruction `here <https://codeburst.io/gitlab-build-and-push-to-a-server-via-ssh-6d27ca1bf7b4>`__

- Clone the repo at the unige server, and use a cronjob to build the docs and move them to the right directory.

- `gitlab pages <https://about.gitlab.com/product/pages/>`_ , however that doesn't work for me (29.11.2018) even with their own examples.


*Page last edited 2018-12-06*
